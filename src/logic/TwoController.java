package logic;

import java.net.URL;
import java.util.ResourceBundle;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class TwoController {

    @FXML
    private Label label, result, answer;

    @FXML
    private Button yes, no;

    @FXML
    private ImageView image1, image2, image3, image4, image5, image6;

    @FXML
    private void tidyAction (ActionEvent event) throws Exception {
        result.setText("Incorrect!");
        answer.setText("The room was far from clean:");
        yes.setDisable(true);
        no.setDisable(true);
        image1.setVisible(true);
        image2.setVisible(true);
        image3.setVisible(true);
        image4.setVisible(true);
        image5.setVisible(true);
        image6.setVisible(true);

    }

    @FXML
    private void farAction (ActionEvent event) throws Exception {
        result.setText("Correct!");
        answer.setText("The room was far from clean:");
        yes.setDisable(true);
        no.setDisable(true);
        image1.setVisible(true);
        image2.setVisible(true);
        image3.setVisible(true);
        image4.setVisible(true);
        image5.setVisible(true);
        image6.setVisible(true);
    }

    @FXML
    private void handleButtonAction (ActionEvent event) throws Exception {

        Stage stage;
        Parent root;

        stage = (Stage) yes.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/templateOne.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

}