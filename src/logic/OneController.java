package logic;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;

public class OneController {
    final int[] i = {4};

    FileInputStream input = new FileInputStream("src/fxml/assets/passive.png");
    Image image = new Image(input);
    ImageView view = new ImageView(image);
    ImageView face = new ImageView((image));

    FileInputStream input1 = new FileInputStream("src/fxml/assets/dwindow.png");
    Image image1 = new Image(input1);

    FileInputStream input2 = new FileInputStream("src/fxml/assets/cbed.png");
    Image image2 = new Image(input2);

    FileInputStream input3 = new FileInputStream("src/fxml/assets/dfloor.png");
    Image image3 = new Image(input3);

    FileInputStream input4 = new FileInputStream("src/fxml/assets/dshelf.png");
    Image image4 = new Image(input4);

    FileInputStream input5 = new FileInputStream("src/fxml/assets/cchair.png");
    Image image5 = new Image(input5);

    FileInputStream input6 = new FileInputStream("src/fxml/assets/cdraws.png");
    Image image6 = new Image(input6);

    @FXML
    private Label label;

    @FXML
    private Button button;

    @FXML
    private ImageView imageView;

    public OneController() throws FileNotFoundException {
    }

    @FXML
    private void handleButtonTransition (ActionEvent event) throws Exception {

        Stage stage;
        Parent root;

        stage = (Stage) button.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/templateTwo.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void firstButton (ActionEvent event) throws Exception {
        if(i[0]>1) {
            imageView.setImage(image1);
            i[0]--;
            label.setText("Views left: " + i[0]);
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void secondButton (ActionEvent event) throws Exception {
        if(i[0]>1) {
            imageView.setImage(image2);
            i[0]--;
            label.setText("Views left: " + i[0]);
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void thirdButton (ActionEvent event) throws Exception {
        if(i[0]>1) {
            imageView.setImage(image3);
            i[0]--;
            label.setText("Views left: " + i[0]);
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void fourthButton (ActionEvent event) throws Exception {
        if(i[0]>1) {
            imageView.setImage(image4);
            i[0]--;
            label.setText("Views left: " + i[0]);
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void fifthButton (ActionEvent event) throws Exception {
        if(i[0]>1) {
            imageView.setImage(image5);
            i[0]--;
            label.setText("Views left: " + i[0]);
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void sixthButton (ActionEvent event) throws Exception {
        if(i[0]>1) {
            imageView.setImage(image6);
            i[0]--;
            label.setText("Views left: " + i[0]);
        }
        else{
            label.setText("Views left: 0");
        }
    }
}