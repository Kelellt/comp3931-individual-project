package logic;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class L1S2Controller {

    @FXML
    private Label label, result, answer, tidy, far;

    @FXML
    private Button yes, no, transbutton;

    @FXML
    private ImageView image1, image2, image3, image4, image5, image6;

    @FXML
    private void tidyAction (ActionEvent event) throws Exception {
        result.setText("Incorrect!");
        answer.setText("The room was far from tidy!");
        tidy.setText("These sections are tidy:");
        far.setText("These sections are far from:");
        yes.setDisable(true);
        no.setDisable(true);
        transbutton.setVisible(true);
        image1.setVisible(true);
        image2.setVisible(true);
        image3.setVisible(true);
        image4.setVisible(true);
        image5.setVisible(true);
        image6.setVisible(true);

    }

    @FXML
    private void farAction (ActionEvent event) throws Exception {
        result.setText("Correct!");
        answer.setText("The room was far from tidy!");
        tidy.setText("These sections are tidy:");
        far.setText("These sections are far from:");
        yes.setDisable(true);
        no.setDisable(true);
        transbutton.setVisible(true);
        image1.setVisible(true);
        image2.setVisible(true);
        image3.setVisible(true);
        image4.setVisible(true);
        image5.setVisible(true);
        image6.setVisible(true);
    }

    @FXML
    private void handleButtonAction (ActionEvent event) throws Exception {

        Stage stage;
        Parent root;

        stage = (Stage) yes.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/level1stage1.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

}