package logic;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class InstructionsController {

    @FXML
    private Label label;

    @FXML
    private Button transbutton;

    @FXML
    private void handleButtonAction (ActionEvent event) throws Exception {

        Stage stage;
        Parent root;

        stage = (Stage) transbutton.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/level1Intro.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

}
