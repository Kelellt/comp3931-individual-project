package logic;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class L5IntroController {

    @FXML
    private Label label;

    @FXML
    private Button transbutton;

    @FXML
    private void handleButtonAction (ActionEvent event) throws Exception {

        Stage stage;
        Parent root;
        stage = (Stage) transbutton.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/level5stage1.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/fxml/CSS/style.css");
        //scene.getStylesheets().addAll(this.getClass().getResource("/fxml/CSS/style.css").toExternalForm());
        stage.setScene(scene);
        stage.show();

    }

}