package logic;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class L3S1Controller {
    final int[] i = {4};

    FileInputStream input1 = new FileInputStream("src/fxml/assets/L3I1.png");
    Image image1 = new Image(input1);

    FileInputStream input2 = new FileInputStream("src/fxml/assets/L3I2.png");
    Image image2 = new Image(input2);

    FileInputStream input3 = new FileInputStream("src/fxml/assets/L3I3.png");
    Image image3 = new Image(input3);

    FileInputStream input4 = new FileInputStream("src/fxml/assets/L3I4.png");
    Image image4 = new Image(input4);

    FileInputStream input5 = new FileInputStream("src/fxml/assets/L3I5.png");
    Image image5 = new Image(input5);

    FileInputStream input6 = new FileInputStream("src/fxml/assets/L3I6.png");
    Image image6 = new Image(input6);

    @FXML
    private Label label, lview;

    @FXML
    private Button button1, button2, button3, button4, button5, button6,transbutton;

    @FXML
    private ImageView imageView;

    public L3S1Controller() throws FileNotFoundException {
    }

    @FXML
    private void handleButtonTransition (ActionEvent event) throws Exception {
        Stage stage;
        Parent root;

        stage = (Stage) transbutton.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/level3stage2.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    private void firstButton (ActionEvent event) throws Exception {
        if(i[0]>0) {
            imageView.setImage(image1);
            lview.setText("Image View: Bed");
            button1.setDisable(true);
            i[0]--;
            label.setText("Views left: " + i[0]);
            if(i[0]==0) {
                transbutton.setVisible(true);
            }
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void secondButton (ActionEvent event) throws Exception {
        if(i[0]>0) {
            imageView.setImage(image2);
            lview.setText("Image View: Floor");
            button2.setDisable(true);
            i[0]--;
            label.setText("Views left: " + i[0]);
            if(i[0]==0) {
                transbutton.setVisible(true);
            }
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void thirdButton (ActionEvent event) throws Exception {
        if(i[0]>0) {
            imageView.setImage(image3);
            lview.setText("Image View: Under Bed");
            button3.setDisable(true);
            i[0]--;
            label.setText("Views left: " + i[0]);
            if(i[0]==0) {
                transbutton.setVisible(true);
            }
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void fourthButton (ActionEvent event) throws Exception {
        if(i[0]>0) {
            imageView.setImage(image4);
            lview.setText("Image View: Desk");
            button4.setDisable(true);
            i[0]--;
            label.setText("Views left: " + i[0]);
            if(i[0]==0) {
                transbutton.setVisible(true);
            }
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void fifthButton (ActionEvent event) throws Exception {
        if(i[0]>0) {
            imageView.setImage(image5);
            lview.setText("Image View: Bookcase");
            button5.setDisable(true);
            i[0]--;
            label.setText("Views left: " + i[0]);
            if(i[0]==0) {
                transbutton.setVisible(true);
            }
        }
        else{
            label.setText("Views left: 0");
        }
    }

    @FXML
    private void sixthButton (ActionEvent event) throws Exception {
        if(i[0]>0) {
            imageView.setImage(image6);
            lview.setText("Image View: Shoe Rack");
            button6.setDisable(true);
            i[0]--;
            label.setText("Views left: " + i[0]);
            if(i[0]==0) {
                transbutton.setVisible(true);
            }
        }
        else{
            label.setText("Views left: 0");
        }
    }
}