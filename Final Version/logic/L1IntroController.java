package logic;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class L1IntroController {

    @FXML
    private Button transbutton;

    @FXML
    private void handleButtonAction (ActionEvent event) throws Exception {

        Stage stage;
        Parent root;
        stage = (Stage) transbutton.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/fxml/level1stage1.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/fxml/CSS/style.css");
        stage.setScene(scene);
        stage.show();

    }

}